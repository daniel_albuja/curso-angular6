import { Component, OnInit,Input,HostBinding,HostListener } from '@angular/core';
import { LoginObjeto } from './../models/loginObjeto.model';
@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})

export class ListadoComponent implements OnInit {
	
	preguntas: LoginObjeto;
	  	constructor() { 
	  
	}
    ngOnInit(): void {
  		this.preguntas=new LoginObjeto();
  	}	

  	recargar():void{
  		window.location.reload();
  	}



}
