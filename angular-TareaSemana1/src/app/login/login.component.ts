import { Component, OnInit,ViewChild } from '@angular/core';
import { Usuario } from './../models/usuario.model';
import { LoginObjeto } from './../models/loginObjeto.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	listaRanking: Usuario[];
	div1:boolean=false;
  div2:boolean=true;
	destinos: string[];
	closeResult = '';
  constructor() { 
  	this.listaRanking = [];
  }

  ngOnInit(): void {
  	
  }

	empezar(nombre:string):boolean{
		if(!nombre){
			
		}else{
		  this.div1=true;
      this.div2=false;
  		this.listaRanking.push(new Usuario(nombre));
  		console.log(this.listaRanking);
  		
	}
	
    return false;
  }



}
