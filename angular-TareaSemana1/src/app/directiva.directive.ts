import { Directive,  HostBinding,HostListener} from '@angular/core';

@Directive({
  selector: '[appDirectiva]'
})
export class DirectivaDirective {
	possibleColores=[
		'darksalmon','hotpink','lightskyblue','goldenrod','blue','green','cornflowerblue','lightslategrey','black','blueviolet','crimson','magenta','sienna'
	];
	@HostBinding('style.color') color:string;
	@HostBinding('style.border-color') colorBorde:string
  	@HostListener('mouseover') nuevoColor(){
  		const colorSeleccion=Math.floor(Math.random()*this.possibleColores.length);
  		this.color=this.colorBorde=this.possibleColores[colorSeleccion];
  	}
  constructor() { }

}
