export class Usuario{
	public id:number;
	public usuario:string;
	public tiempoInicio:Date;
	public tiempoFin:Date;
	public tiempoRanking:Date;
	public ranking:any;


	constructor(u:string){
		this.usuario=u;
		this.tiempoInicio=new Date();
	}

}